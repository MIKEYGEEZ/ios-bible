//
//  ChapterTableViewController.h
//  BibleReader
//
//  Created by Student on 4/16/13.
//  Copyright (c) 2013 Harding University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChapterTableViewController : UITableViewController
@property (nonatomic,strong) NSArray *chaptersOfBook;
@property (nonatomic,strong) NSString *nameOfBook;
@property (nonatomic,strong) NSString *indexOfBook;
@end
