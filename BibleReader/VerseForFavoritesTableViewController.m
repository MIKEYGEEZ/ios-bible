//
//  VerseForFavoritesTableViewController.m
//  BibleReader
//
//  Created by Student on 4/20/13.
//  Copyright (c) 2013 Harding University. All rights reserved.
//

#import "VerseForFavoritesTableViewController.h"

@interface VerseForFavoritesTableViewController ()

@end

@implementation VerseForFavoritesTableViewController
@synthesize versesInChapter = _versesInChapter;
@synthesize nameOfBook = _nameOfBook;
@synthesize indexOfBook = _indexOfBook;
@synthesize nameOfChapter = _nameOfChapter;
@synthesize checkedCells = _checkedCells;

- (void)setVersesInChapter:(NSArray *)versesInChapter {
    if (_versesInChapter != versesInChapter) {
        _versesInChapter = versesInChapter;
        [self.tableView reloadData];
    }
}

- (IBAction)doneAddingFavoriteVersesPressed:(UIBarButtonItem *)sender {
//    NSArray *sortedCheckedCells = [self.checkedCells sortedArrayUsingComparator:^NSComparisonResult(id cell1, id cell2) {
//        return ((NSIndexPath *)cell1).row > ((NSIndexPath *)cell2).row;
//    }];
    
    self.navigationItem.prompt = @"Favorites Added!";
    [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(restoreDefaultPrompt) userInfo:nil repeats:NO ];
    
    
    
}

- (void)restoreDefaultPrompt {
    self.navigationItem.prompt = @"Add Favorite Verses"; 
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.checkedCells = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.versesInChapter count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"verseCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    NSString* verseName = [[[self.versesInChapter objectAtIndex:indexPath.row] allKeys] objectAtIndex:0];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",[verseName substringToIndex:5], [verseName substringFromIndex:5]];
    cell.detailTextLabel.text = [[self.versesInChapter objectAtIndex:indexPath.row] objectForKey:verseName];
    
    if ( [self.checkedCells indexOfObject:indexPath] == NSNotFound ) {
        cell.accessoryType = UITableViewCellAccessoryNone;
    } else {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return [NSString stringWithFormat:@"%@ %@ %@", self.nameOfBook, [self.nameOfChapter substringToIndex:7], [self.nameOfChapter substringFromIndex:7]];
    }
    else return @"";
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ( [self.checkedCells indexOfObject:indexPath] == NSNotFound ) {
        //Do Nothing
    } else {
        [self.checkedCells removeObject:indexPath];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ( [self.checkedCells indexOfObject:indexPath] == NSNotFound ) {
        [self.checkedCells addObject:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }    
    
}

@end
