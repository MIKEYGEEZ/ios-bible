//
//  BookTableViewController.m
//  BibleReader
//
//  Created by Student on 4/16/13.
//  Copyright (c) 2013 Harding University. All rights reserved.
//

#import "BookTableViewController.h"
#import "ChapterTableViewController.h"
#define OLD_TESTAMENT_SECTION 0
#define NUM_OLD_TESTAMENT_BOOKS 39
#define NUM_NEW_TESTAMENT_BOOKS 27

@interface BookTableViewController ()

@end

@implementation BookTableViewController
@synthesize oldTestamentBooks = _oldTestamentBooks;
@synthesize neuerTestamentBooks = _neuerTestamentBooks;


- (void)setOldTestamentBooks:(NSArray *)oldTestamentBooks {
    if (_oldTestamentBooks != oldTestamentBooks) {
        _oldTestamentBooks = oldTestamentBooks;
        [self.tableView reloadData];
    }
}

- (void)setNeuerTestamentBooks:(NSArray *)neuerTestamentBooks {
    if (_neuerTestamentBooks != neuerTestamentBooks) {
        _neuerTestamentBooks = neuerTestamentBooks;
        [self.tableView reloadData];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"goToChapters"]) {
        
        NSIndexPath *selectedRowIndexPath = self.tableView.indexPathForSelectedRow;
        NSString *nameOfSelectedBook;
        NSArray *chaptersInBook;
        if (selectedRowIndexPath.section == OLD_TESTAMENT_SECTION) {
            //the book selected is in the old testament
            
            nameOfSelectedBook = [[[self.oldTestamentBooks objectAtIndex:selectedRowIndexPath.row] allKeys] objectAtIndex:0];
            chaptersInBook = [[self.oldTestamentBooks objectAtIndex:selectedRowIndexPath.row] valueForKey:nameOfSelectedBook];
            
        }
        else {
            //the selected book is in the new testament
            
            nameOfSelectedBook = [[[self.neuerTestamentBooks objectAtIndex:selectedRowIndexPath.row] allKeys] objectAtIndex:0];
            chaptersInBook = [[self.neuerTestamentBooks objectAtIndex:selectedRowIndexPath.row] valueForKey:nameOfSelectedBook];
        }
        
        [segue.destinationViewController setChaptersOfBook:chaptersInBook];
        [segue.destinationViewController setNameOfBook:nameOfSelectedBook];
        [segue.destinationViewController setIndexOfBook:[NSString stringWithFormat:@"Book%d",selectedRowIndexPath.row +1]] ;
        [segue.destinationViewController setTitle:nameOfSelectedBook];
    }
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

+ (id)getJSONArrayFromTextFileWithFileName: (NSString *)fileName {
    
    NSString *filepath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"txt"];
    NSData *contents = [NSData dataWithContentsOfFile:filepath options:0 error:nil];    
    return [NSJSONSerialization JSONObjectWithData:contents options:0 error:nil ];
}

- (void)viewWillAppear:(BOOL)animated {
    
    NSArray *bothTestaments = [BookTableViewController getJSONArrayFromTextFileWithFileName:@"jsonBible"];
    self.oldTestamentBooks = [bothTestaments subarrayWithRange:NSMakeRange(0, NUM_OLD_TESTAMENT_BOOKS)];
    self.neuerTestamentBooks = [bothTestaments subarrayWithRange:NSMakeRange(NUM_OLD_TESTAMENT_BOOKS, NUM_NEW_TESTAMENT_BOOKS)];
    self.title = @"The Bible";
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return (section == OLD_TESTAMENT_SECTION) ? [self.oldTestamentBooks count] : [self.neuerTestamentBooks count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"book";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    if (indexPath.section == OLD_TESTAMENT_SECTION) {
        cell.textLabel.text = [[[self.oldTestamentBooks objectAtIndex:indexPath.row] allKeys]objectAtIndex:0];
    }
    else {
        //new Testament is in view, use new Testament books
        cell.textLabel.text = [[[self.neuerTestamentBooks objectAtIndex:indexPath.row] allKeys]objectAtIndex:0];
    }
    
    return cell;
}

- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return (section == OLD_TESTAMENT_SECTION) ? @"Old Testament" : @"New Testament";
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
