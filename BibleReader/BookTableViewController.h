//
//  BookTableViewController.h
//  BibleReader
//
//  Created by Student on 4/16/13.
//  Copyright (c) 2013 Harding University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookTableViewController : UITableViewController

@property (strong, nonatomic) NSArray *oldTestamentBooks;
@property (strong, nonatomic) NSArray *neuerTestamentBooks;
+ (id)getJSONArrayFromTextFileWithFileName: (NSString *)fileName;

@end
